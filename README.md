# VUE-BOTTOM-SHEET

![vue2](https://img.shields.io/badge/vue-2.6.x-brightgreen.svg)
![free](https://img.shields.io/badge/open-source-red.svg)
![ver](https://img.shields.io/badge/ver-0.1.0-blue.svg)

The `v-bottom-sheet` component is draggable bottom sheet dialog with gesture support.

## Table of Contents
- **[Demo](#demo)**
- **[Installation](#install)**
- **[Usage](#usage)**
    - [Basic example](#basic-example)
    - [Props](#props)
    - [Slots](#slots)
- **[Important notes](#important-notes)**
- **[License](#license)**

## Demo
> Bottom sheet component has half and full state for displaying and scrollable content slot.
>
> **[Demo Link](https://foxy-dev.gitlab.io/vue-bottom-sheet/)**.

## Installation

This project uses `node` and `yarn`. Go check them out if you don't have them locally installed.

#### Project setup
```
$ yarn install
```

#### Compiles and hot-reloads for development
```
$ yarn serve
```

#### Compiles and minifies for production
```
$ yarn build
```

#### Lints and fixes files
```
$ yarn lint
```

---

## Usage

>`v-bottom-sheet` is a bottom sheet component for display any kinds of dialogs.
>As the user drag, `v-bottom-sheet` will change its state according to direction of users gesture.

### Basic example

Use the scoped slot to set custom close button:

```html
<!--App.vue-->
<template>
    <v-bottom-sheet
        dismiss
        initial="half"
    >
        <template #close="{ close }">
            <button @click="close()">close dialog</button>
        </template>
        <template #default>
            <!-- content -->
        </template>
    </v-bottom-sheet>
</template>

<!--App.vue-->
<script>
import VBottomSheet from '@/components/v-bottom-sheet';

export default {
    components: { VBottomSheet }
}
</script>
```

### Props

Name       | Required |      Type         | Default     | Description 
:----      | :----:   | :----:            |:----:       |:----   
`:dismiss` | `false`  | `[Boolean]`       | `false`     | Should dialog have a close button.
`:overlay` | `false`  | `[Boolean]`       | `true`      | Should it show overlay layer.
`:initial` | `false`  | `[String]`        | `half`      | Set initial state for dialog `[half, full, closed]`.

### Slots
| Name       | Props       | Props type | Description |
| :--------- | :--------: | :--------: | :------------   
| `#default` | `none`     |            | Place were your content should be.
| `#close`   | `close`    | `function` | Place for close dialog button with `close` handler in scoped slot data.

## Important notes

- **⚠️ It's just a demo version of `v-bottom-sheet` component.**
- **⚠️ Hammer.js supports only by modern browsers.**
---

## License

[![MIT](https://img.shields.io/badge/license-MIT-green.svg)](http://opensource.org/licenses/MIT)

© Foxy Dev