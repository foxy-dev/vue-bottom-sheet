/**
 * @typedef {string} SheetStateType
 */
/**
 * Component name and CSS class
 * @type {string}
 */
export const BASE_CLASS = 'v-bottom-sheet';
/**
 * Full open state
 * @type {SheetStateType}
 */
export const STATE_FULL = 'full';
/**
 * Half open state
 * @type {SheetStateType}
 */
export const STATE_MOVE = 'move';
/**
 * Closed state
 * @type {SheetStateType}
 */
export const STATE_CLOSED = 'closed';
/**
 * Move state
 * @type {SheetStateType}
 */
export const STATE_HALF = 'half';

export default {
    BASE_CLASS,
    STATE_FULL,
    STATE_HALF,
    STATE_MOVE,
    STATE_CLOSED,
};
