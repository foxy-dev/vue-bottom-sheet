import '../styles/v-bottom-sheet.scss';
import * as Hammer from 'hammerjs';

import {
    BASE_CLASS,
    STATE_FULL,
    STATE_HALF,
    STATE_MOVE,
    STATE_CLOSED,
} from './constants';

/**
 * @emits closed - when sheet is closed
 * @emits half - when sheet open on a half
 * @emits full - when sheet is full size
 *
 * @slot close - for close button
 * @slot content - for content in sheet
 */
export default {
    name: BASE_CLASS,

    props: {
        /**
         * Initial state property
         */
        initial: {
            type: String,
            required: false,
            default: STATE_CLOSED,
            validator: (value) =>
                [ STATE_FULL, STATE_HALF, STATE_CLOSED ].includes(value),
        },
        /**
         * Should can be closed by click on close button
         */
        dismiss: {
            type: Boolean,
            default: false,
        },

        /**
         * Should it show overlay layer
         */
        overlay: {
            type: Boolean,
            default: true,
        },
    },

    data () {
        return {
            /**
             * Hammer controller instance
             * @type (null|Object)
             */
            gestureController: null,
            /**
             * Last position by Y-axis
             * @type {Number}
             */
            lastPosition: 0,
            /**
             * Component css class name
             * @type {string}
             */
            baseClass: BASE_CLASS,
            /**
             * @type SheetStateType
             */
            state: this.initial,
        };
    },

    watch: {
        state: 'updateSheet',
    },

    computed: {
        showDismissSlot () {
            if (!this.dismiss) return false;

            return ![ STATE_CLOSED, STATE_MOVE ].includes(this.state);
        },
    },

    mounted () {
        this.updateSheet();
        this.initDragListeners();
    },

    beforeDestroy () {
        this.destroyDragListeners();
    },

    methods: {
        /**
         * Add Hammer instance and event listeners
         */
        initDragListeners () {
            const vm = this;
            const { touchStart, touchEnd } = this;
            const { sheet, header } = this.$refs;

            vm.lastPosition = sheet.clientHeight;
            this.gestureController = new Hammer(header);

            this.gestureController
                .get('pan')
                .set({ direction: Hammer.DIRECTION_VERTICAL });

            this.gestureController.on('panup', (event) => {
                sheet.style.maxHeight = Math.abs(event.deltaY) + vm.lastPosition + 'px';
            });

            this.gestureController.on('pandown', (event) => {
                sheet.style.maxHeight = vm.lastPosition - event.deltaY + 'px';
            });

            this.gestureController.on('panstart', touchStart);

            this.gestureController.on('panend', touchEnd);
        },
        /**
         * Remove Hammer instance and destroy all listeners
         */
        destroyDragListeners () {
            if (!this.gestureController) return;

            this.gestureController.destroy();
            this.gestureController = null;
        },
        /**
         * Fires after user complete gesture
         * @param {Number} delta - amount of gesture delta
         */
        touchEnd ({ deltaY: delta }) {
            let { state } = this;
            const { sheet } = this.$refs;
            const height = sheet.clientHeight;
            const half = window.innerHeight / 2;

            if (height < half - 35 && delta > 0) {
                state = STATE_CLOSED;
            } else if (height <= half + 35 || delta > 0) {
                state = STATE_HALF;
            } else if (height > half + 25) {
                state = STATE_FULL;
            }

            this.setState(state);
        },
        /**
         * Fires when user begin gesture
         */
        touchStart () {
            const { sheet } = this.$refs;
            this.state = STATE_MOVE;
            this.removeModifiers(sheet);
        },
        /**
         * Close bottom sheet
         */
        closeSheet () {
            const { sheet } = this.$refs;

            this.removeModifiers(sheet);

            this.setState(STATE_CLOSED);
        },
        /**
         * Set state value
         * @param {SheetStateType} value - current state
         */
        setState (value) {
            this.state = value;
        },
        /**
         * Set bottom sheet state position
         * @param {SheetStateType} state - current state
         */
        setSheet (state) {
            const vm = this;
            const { sheet } = this.$refs;
            const timeout = state === STATE_CLOSED ? 450 : 150;

            sheet.removeAttribute('style');
            sheet.classList.add(`${BASE_CLASS}__wrap--${state}`);

            setTimeout(() => {
                vm.lastPosition = sheet.clientHeight;
            }, timeout);
        },
        /**
         * Set overlay state class modifiers
         * @param {SheetStateType} state - current state
         */
        setOverlay (state) {
            const { overlay } = this.$refs;

            const full = `${BASE_CLASS}__overlay--${STATE_FULL}`;
            const half = `${BASE_CLASS}__overlay--${STATE_HALF}`;

            switch (state) {
                case STATE_HALF:
                    overlay.classList.add(half);
                    overlay.classList.remove(full);
                    break;
                case STATE_FULL:
                    overlay.classList.add(full);
                    overlay.classList.remove(half);
                    break;
                default:
                    overlay.classList.remove(full, half);
            }
        },
        /**
         * Update sheet state according to gesture direction and coords
         * @param {SheetStateType} arg
         */
        updateSheet (arg = this.state) {
            if (arg === STATE_MOVE) return;

            this.setSheet(arg);
            this.setOverlay(arg);
            this.$emit(arg);
        },
        /**
         * Remove all BEM modifiers from element
         * @param {Element} el - target element
         */
        removeModifiers (el) {
            const list = [ ...el.classList ];

            el.classList.remove(...list.filter((name) => name.includes('--')));
        },
    },

    render (h) {
        const { $slots, $scopedSlots, closeSheet, showDismissSlot } = this;
        const makeClass = (type) => ({ staticClass: type ? `${BASE_CLASS}__${type}` : BASE_CLASS });

        const closeContent = () => {
            const closeSymbol = h('span', { attrs: { ariaHidden: 'true' } }, '×');
            const closeBtn = h('button', {
                ...makeClass('close'),
                attrs: { type: 'button', ariaLabel: 'Close' },
                on: { click: closeSheet },
            }, [ closeSymbol ]);

            if (!showDismissSlot) return;

            return h('transition', {
                props: {
                    name: `${BASE_CLASS}__close-animation`,
                    mode: 'out-in',
                },
            }, [ $scopedSlots.close ? $scopedSlots.close({ close: closeSheet }) : closeBtn ]);
        };
        const content = h('div', makeClass('content'), [ $slots.default ]);
        const handler = h('span', makeClass('handler'));
        const overlay = h('div', {
            ...makeClass('overlay'), ref: 'overlay', on: {
                click: () => {
                    if (this.dismiss) closeSheet()
                },
            },
        });
        const header = h('div', { ...makeClass('header'), ref: 'header' }, [ handler, closeContent() ]);
        const sheet = h('div', { ...makeClass('wrap'), ref: 'sheet' }, [ header, content ]);

        const components = [];
        if (this.overlay) {
            components.push(overlay);
        }
        components.push(sheet);

        return h('div', { staticClass: BASE_CLASS, refInFor: true }, components);
    },
};
